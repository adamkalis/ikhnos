#!/usr/bin/env python

import sys
from PIL import Image
import ephem
import datetime
import matplotlib.pyplot as plt
import numpy as np
import requests
import json
import shutil
from mutagen.oggvorbis import OggVorbis
import re
import os.path
import argparse
import pathlib
from matplotlib.offsetbox import AnchoredText

parser = argparse.ArgumentParser(description='Analyze observations')

parser.add_argument('observations', nargs='+', type=int,
                    help='One or more observation IDs from SatNOGS Network')
parser.add_argument('-t', '--tle-path', nargs='?', const='tle', default='tle', type=pathlib.Path,
                    help='Path of the TLE file, if not set the default value is "tle" in the current directory')
parser.add_argument('-f', '--frequency-offset', nargs='?', const=0.0, default=0.0, type=float,
                    help='Offset in KHz to add to the expected center frequency, this can be negative or positive number, default value: 0.0')
parser.add_argument('-r', '--frequency-range', nargs='?', const=24.0, default=24.0, type=float, choices=[24.0, 25, 29.0, 33.2, 38.5, 77.0, 115.2, 230.4],
                    help='The plus-minus KHz from the centered frequency as it is in the X axis in observation waterfall, default value: 24.0')
parser.add_argument('-e', '--tle-epoch-threshold', nargs='?', const=5, default=5, type=int,
                    help='Set the threshold of the difference of observation start time and TLE epoch in days, default value: 5')
parser.add_argument('-C', '--keep-created-files', action='store_true', help='Keep files created by analysis')
parser.add_argument('-A', '--keep-audio', action='store_true', help='Keep downloaded audio file')
parser.add_argument('-W', '--keep-waterfall', action='store_true', help='Keep downloaded waterfall file')
parser.add_argument('-v', '--verbose', action='store_true', help='Be more verbose')

args = parser.parse_args()
plt.switch_backend('Agg')

for observation_id in args.observations:
    obs_id = str(observation_id)
    print("Requesting observation " + obs_id)
    r = requests.get('https://network.satnogs.org/api/observations/' + obs_id + '/?format=json')
    observation = json.loads(r.content.decode('utf8'))
    if args.verbose:
        print(observation)


    print("Downloading the observation page for getting frequency and tle")
    observation_link = "https://network.satnogs.org/observations/" + obs_id
    obs_html = requests.get(observation_link).content
    tle_regex = r"<pre.*>(1 .*)<br>(2 .*)</pre>"
    tle_matches = re.search(tle_regex, obs_html.decode("utf-8"))
    freq_regex = r"(\d*\.\d*) MHz"
    freq_matches = re.findall(freq_regex, obs_html.decode("utf-8")).pop()

    audio_path = obs_id + '.ogg'
    waterfall_path = obs_id + '.png'

    if not os.path.isfile(audio_path):
        if observation['payload']:
            print("Downloading " + observation['payload'] + ' for getting the right duration')
            observation_ogg_data = requests.get(observation['payload'], stream=True).content
        elif observation['archive_url']:
            print("Downloading " + observation['archive_url'] + ' for getting the right duration')
            observation_ogg_data = requests.get(observation['archive_url'], stream=True).content
        else:
            print("No audio file skipping observation " + obs_id)
            continue
        with open(audio_path, 'wb') as out_file:
            out_file.write(observation_ogg_data)

    if not os.path.isfile(waterfall_path):
        print("Downloading " + observation['waterfall'] + ' for the background image')
        img_data = requests.get(observation['waterfall']).content
        with open(waterfall_path, 'wb') as handler:
            handler.write(img_data)

    # Set tle
    tle0 = "Estimated TLE"
    tle1 = tle_matches.group(1)
    tle2 = tle_matches.group(2)
    satellite1 = ephem.readtle(tle0, tle1, tle2)

    observer = ephem.Observer()
    observer.lat = str(observation['station_lat'])
    observer.lon = str(observation['station_lng'])
    observer.elevation = observation['station_alt']
    freq0 = float(freq_matches)
    tstart = observation['start'].replace('Z', '')
    start = datetime.datetime.strptime(tstart, "%Y-%m-%dT%H:%M:%S")
    ystart = datetime.datetime(start.year, 1, 1)
    diff = start-ystart
    #+1 day in timedelta as TLE show the day and a fraction of it so Jan 1st is 1 not 0.
    obs_epoch_day = ((start - ystart).total_seconds() + datetime.timedelta(days=1).total_seconds()) / datetime.timedelta(days=1).total_seconds()
    f = OggVorbis(audio_path)
    nseconds = int(round(f.info.length))

    tle_file = args.tle_path
    with open(tle_file) as f:
        tle_lines = f.read().splitlines()
    tles = []
    for i in range(0, len(tle_lines), 3):
         tles.append(tle_lines[i:i + 3])

    tle_sets_read = 0
    for tba_tle in tles:
        overlay_path = obs_id + ' ' + tba_tle[0].replace('/','_') + "_freq_diff.png"
        satellite2 = ephem.readtle(tba_tle[0], tba_tle[1], tba_tle[2])
        epoch_regex = r".{20}\s*(\d*\.\d*)"
        epoch_matches = re.search(epoch_regex, tba_tle[1])
        epoch_day = epoch_matches.group(1)
        obs_tle_epochs_diff = float(epoch_day) - obs_epoch_day
        bef_after = 'B'
        if obs_tle_epochs_diff > 0:
            bef_after = 'A'
        if abs(obs_tle_epochs_diff) > args.tle_epoch_threshold:
            print("TLE too old/new: Epoch differs by more than {:.1f} days\n"
                  "from observation start time (threshold: {:.1f} days)".format(
                    obs_tle_epochs_diff,
                    args.tle_epoch_threshold))
            continue
        tle_sets_read += 1
        print('%s - %s - %s' % (tle_sets_read, obs_tle_epochs_diff, bef_after))

        times = [start+datetime.timedelta(seconds=s) for s in range(0, nseconds)]

        v1 = []
        v2 = []
        for t in times:
            observer.date = t
            satellite1.compute(observer)
            satellite2.compute(observer)
            v1.append(satellite1.range_velocity)
            v2.append(satellite2.range_velocity)

        freq1 = freq0*(1.0-np.array(v1)/299792458.0)
        freq2 = freq0*(1.0-np.array(v2)/299792458.0)
        dfreq = (freq2-freq1)*1000.0 + args.frequency_offset

        t = np.arange(nseconds)

        fig = plt.figure(figsize=(10 * 0.8,20))
        ax = fig.add_subplot(111)
        ax.plot(dfreq, t, color='red', linewidth=0.5)
        ax.set_ylabel("Time (seconds)")
        ax.set_xlabel("Frequency (kHz)")
        ax.set_xlim(-1 * args.frequency_range, args.frequency_range)
        ax.set_ylim(0, nseconds)
        ax.tick_params(axis='x', colors='red')
        ax.tick_params(axis='y', colors='red')

        fig_label = f"SatNOGS Observation {obs_id}"
        at = AnchoredText(fig_label, prop=dict(size=15), frameon=True, loc='upper left')
        at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
        ax.add_artist(at)

        fig.savefig(overlay_path, bbox_inches="tight", transparent=True)
        plt.close(fig)

        background = Image.open(waterfall_path)
        overlay = Image.open(overlay_path)

        background = background.convert("RGBA")
        overlay = overlay.convert("RGBA")

        ov = Image.new('RGBA',background.size, (0, 0, 0, 0))
        ov.paste(overlay, (0,0))
        background.paste(ov, (0,0), ov)
        if not os.path.exists(obs_id):
            os.makedirs(obs_id)
        obj_regex = r"1 (.....). (........)"
        [(norad_id, cospar_id)] = re.findall(obj_regex, tba_tle[1])
        background.save(obs_id + '/' + tstart + '_' + obs_id + '_' + norad_id + '_' + cospar_id.replace(' ', '') + '_'+ tba_tle[0].replace(' ','-').replace('/','_') + '_' + str(abs(obs_tle_epochs_diff)) + '_' + bef_after + '_r' + str(args.frequency_range) + '_f' + str(args.frequency_offset) + ".png","PNG")
        background.close()
        overlay.close()
        if not args.keep_created_files:
            os.remove(overlay_path)
    if not args.keep_audio:
       os.remove(audio_path)
    if not args.keep_waterfall:
       os.remove(waterfall_path)
